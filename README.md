# OpenML dataset: red_wine

https://www.openml.org/d/44972

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Data Description**

The data were collected from May/2004 to February/2007 using only protected designation of origin samples that were tested at the official certification entity (CVRVV). The CVRVV is an inter-professional organization with the goal of improving the quality and marketing of vinho verde. The data were recorded by a computerized system (iLab), which automatically manages the process of wine sample testing from producer requests to laboratory and sensory analysis. Each entry denotes a given test (analytical or sensory).
This dataset contains only the read wine.

The goal is using chemical analysis determine the quality of the wine.

**Attribute Description**

1. *fixed_acidity*
2. *volatile_acidity*
3. *citric_acid*
4. *residual_sugar*
5. *chlorides*
6. *free_sulfur_dioxide*
7. *total_sulfur_dioxide*
8. *density*
9. *pH*
10. *sulphates*
11. *alcohol*
12. *quality* - target feature

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44972) of an [OpenML dataset](https://www.openml.org/d/44972). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44972/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44972/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44972/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

